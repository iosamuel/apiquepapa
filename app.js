var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var firebase = require('firebase');
var admin = require('firebase-admin');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({
	'origin': '*'
}));

var serviceAccount = require('./quepapa-d3c45-firebase-adminsdk-8dbhg-06d1fc6607.json');
admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
	databaseURL: "https://quepapa-d3c45.firebaseio.com/"
});
firebase.initializeApp({
	apiKey: "AIzaSyA7kUQZIy5LcMByA8kO9TvLTc1nH6D9dyU",
    authDomain: "quepapa-d3c45.firebaseapp.com",
    databaseURL: "https://quepapa-d3c45.firebaseio.com",
    storageBucket: "quepapa-d3c45.appspot.com",
    messagingSenderId: "133684086613"
});

var routes = require('./routes')(app, admin, firebase);

var server = app.listen(process.env.PORT || 3000, function(){
	console.log('Listening on port %s...', server.address().port);
});
