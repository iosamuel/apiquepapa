var _ = require('lodash');
var ipsOrder = {};

var appRouter = function(app, admin, firebase){
	var adminDB = admin.database();
	var adminAuth = admin.auth();
	var clientDB = firebase.database();
	var clientAuth = firebase.auth();

	/* GET */
	app.get('/base_price', function(req, res){
		adminDB.ref('base_price').once('value', function(snap){
			res.send({base_price: snap.val()});
		});
	});
	app.get('/toppings', function(req, res){
		adminDB.ref('toppings').once('value', function(snap){
			res.send(snap.val());
		});
	});
	app.get('/drinks', function(req, res){
		let drinks_personal, drinks_group;
		adminDB.ref('drinks_personal').once('value', function(snap){
			drinks_personal = snap.val();

			adminDB.ref('drinks_group').once('value', function(snap){
				drinks_group = snap.val();

				res.send({drinks_personal: drinks_personal, drinks_group: drinks_group});				
			});
		});
	});
	app.get('/orders_length', function(req, res){
		if (req.query.uid) {
			adminDB.ref('users/'+req.query.uid).once('value', function(snap){
				let user_data = snap.val();
				if (user_data && user_data.role && user_data.role == 'caja') {
					adminDB.ref('orders_local/'+user_data.local).once('value', function(snaps){
						let orders = snaps.val();
						let ordersLength = Object.keys(orders).length;

						adminDB.ref('orders_delivery/'+user_data.city).once('value', function(snapsh){
							orders = snapsh.val();
							ordersLength += Object.keys(orders).length;

							res.send({ordersLength: ++ordersLength});
						});
					});
				} else if (user_data && !user_data.role) {
					res.send({ordersLength: ++user_data.orders});
				} else {
					res.send({ordersLength: 1});
				}
			});
		} else {
			res.send({ordersLength: 1});
		}
		
	});

	app.get('*', function(req, res){
		res.send({error: '404', message: 'Pagina no encontrada'});
	});

	/* POST */
	app.post('/login', function(req, res){
		if (req.body.cc && req.body.password){
			clientAuth.signInWithEmailAndPassword(req.body.cc+"@quepapa.co", req.body.password).catch(function(error){
				return res.send({error: error.code, message: error.message});
			}).then(function(user){
				let user_data;
				clientDB.ref('users/'+user.uid).once('value', function(snap){
					user_data = snap.val();
					return res.send({uid: user.uid, user: user_data});
				});
			}, function(error){
				return res.send({error: true, message: error});
			});
		} else {
			return res.send({error: true, message: "Ha habido un error. Revisa los campos."});
		}
	});

	app.post('/order', function(req, res){
		if (req.body.uid) {
			adminDB.ref('users/'+req.body.uid).once('value', function(snap){
				let user_data = snap.val();
				let city = req.body.city;
				if (user_data && user_data.role && user_data.role == 'caja'){
					city = user_data.city;
					adminDB.ref('orders_local').child(user_data.local).push(req.body).catch(function(error){
						return res.send({error: error.code, message: error.message});
					}).then(function(){
						return res.send({ok: true});
					}, function(error){
						return res.send({error: true, message: error});
					});
				} else {
					if ((((new Date) - new Date(JSON.parse(user_data.lastOrder))) >= 300000)) {
						adminDB.ref('orders_delivery').child(city).push(_.pickBy(req.body, (v, k) => k != 'city' && k != 'total')).catch(function(error){
							return res.send({error: error.code, message: error.message});
						}).then(function(){
							if (user_data && user_data.orders >= 0) {
								adminDB.ref('users/'+req.body.uid+'/orders').set(++user_data.orders);
								adminDB.ref('users/'+req.body.uid+'/spended').set((parseInt(user_data.spended) + parseInt(req.body.total)));
								adminDB.ref('users/'+req.body.uid+'/lastOrder').set(JSON.stringify(new Date));
							}
							return res.send({ok: true});
						}, function(error){
							return res.send({error: true, message: error});
						});
					} else {
						res.send({error: true, message: "No puedes ordenar hasta dentro de 5 minutos."});
					}
				}
			});
		} else {
			if (!_.has(ipsOrder, req.ip) || (_.has(ipsOrder, req.ip) && (((new Date) - ipsOrder[req.ip]) >= 300000))) {
				adminDB.ref('orders_delivery').child(req.body.city).push(_.pickBy(req.body, (v, k) => k != 'city' && k != 'uid' && k != 'total')).catch(function(error){
					return res.send({error: error.code, message: error.message});
				}).then(function(){
					ipsOrder[req.ip] = new Date();
					return res.send({ok: true});
				}, function(error){
					return res.send({error: true, message: error});
				});
			} else {
				res.send({error: true, message: "No puedes ordenar hasta dentro de 5 minutos."});
			}
		}	
	});
}

module.exports = appRouter;
